package com.alesmandelj.fishjump;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;

import java.util.Random;

public class FishJump extends ApplicationAdapter {
	SpriteBatch batch;
	Texture background;
	Texture waterImg;
	Texture fishUp;
	Texture fly;
	Texture fishDown;
	Texture gameover;
	Texture flyUp;
	Texture flyDown;
	Texture cloud;
	Texture cloud1;

	float velocity=0;
	float fishY=0;
	float gravity=2;
	Integer gamestate=0;

	float flyX=0;
	float flyY=0;

	Random rand=new Random();

	Circle fishCircle;
	Circle flyCircle;

	int numberClouds=7;
	int flySpeed=17;

	float distanceBetweenClouds;
	float [] CloudX	=new float[numberClouds];
	float[] cloudOffset=new float[numberClouds];

	int flutter=0;
	int score=0;
	String scoreString;
	BitmapFont bitmapFont;
	float cloudSpeed=1;


	public void startGame() {

		fishY = 0;
		flyX = Gdx.graphics.getWidth();
		flyY = (Gdx.graphics.getHeight() - 200) - (rand.nextFloat() * (Gdx.graphics.getHeight() * 3 / 4));
	}

	public void generateClouds(){

		for (int i = 0; i < numberClouds; i++) {

			cloudOffset[i] = (Gdx.graphics.getHeight() - 200) - (rand.nextFloat() * (Gdx.graphics.getHeight() * 3 / 4));
			CloudX[i] = Gdx.graphics.getWidth() / 2 + cloud.getWidth() / 2 - Gdx.graphics.getWidth() / 2 - i * distanceBetweenClouds;
		}
	}




	
	@Override
	public void create () {
		batch = new SpriteBatch();


		background = new Texture("background.png");
		waterImg=new Texture("water.png");
		fishUp=new Texture("fishup.png");
		fly=new Texture("fly.png");
		fishDown=new Texture("fishdown.png");
		gameover=new Texture("gameover.png");

		flyUp=new Texture("flyup.png");
		flyDown=new Texture("flydown.png");

		fishCircle=new Circle();
		flyCircle=new Circle();
		cloud=new Texture("cloud.png");
		cloud1=new Texture("cloud.png");

		scoreString=String.valueOf(score);
		bitmapFont=new BitmapFont();

		bitmapFont.setColor(Color.BLACK);
		bitmapFont.getData().setScale(10);

		flyX=Gdx.graphics.getWidth();
		flyY=(Gdx.graphics.getHeight()-200)-(rand.nextFloat()*(Gdx.graphics.getHeight()*3/4));

		distanceBetweenClouds=Gdx.graphics.getWidth()*2/5;

		generateClouds();
	}

	@Override
	public void render () {
		batch.begin();

		batch.draw(background,0,0,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
		//	batch.draw(waterImg,0,0,Gdx.graphics.getWidth(),300);



		if (flutter==0){

			batch.draw(flyUp,flyX,flyY);
			flutter=1;
		}else{
			batch.draw(flyDown,flyX,flyY);
			flutter=0;

		}


		flyX=flyX-flySpeed;

		for (int i=0; i<numberClouds;i++) {

			if (CloudX[i] > Gdx.graphics.getWidth() + cloud.getWidth()) {

				CloudX[i] -= numberClouds * distanceBetweenClouds;
				cloudOffset[i] = (Gdx.graphics.getHeight()-200)-(rand.nextFloat()*(Gdx.graphics.getHeight()*3/4));

			} else {

				CloudX[i] = CloudX[i] + cloudSpeed;
			}
			batch.draw(cloud, CloudX[i], cloudOffset[i]);
		}

		if (gamestate==0) {
			if (Gdx.input.justTouched()) {

				velocity -= 82;

			}
		}

		if (gamestate==3){
			if (Gdx.input.justTouched()){

				startGame();
				gamestate =0;
				velocity=0;
				score=0;
			}
		}

		velocity=velocity+gravity;
		fishY-=velocity;
		gamestate=1;

		if (fishY<=0){

			velocity=0;
			gravity=0;
			gamestate=0;
		}else{
			gravity=2;
		}

		if (gravity<velocity){

			batch.draw(fishDown,Gdx.graphics.getWidth()/3,fishY);
		}else{

			batch.draw(fishUp,Gdx.graphics.getWidth()/3,fishY);

		}
		batch.draw(waterImg,0,0,Gdx.graphics.getWidth(),150);

		fishCircle.set(Gdx.graphics.getWidth()/3+fishDown.getWidth()/2,fishY+fishDown.getHeight()/2,fishDown.getWidth()/2);
		flyCircle.set(flyX+flyUp.getWidth()/2,flyY+flyUp.getHeight()/2,flyUp.getWidth()/2);


		if (Intersector.overlaps(fishCircle,flyCircle)) {

			flyX=Gdx.graphics.getWidth()+1100;
			flyY=(Gdx.graphics.getHeight()-200)-(rand.nextFloat()*(Gdx.graphics.getHeight()*3/4));

			score++;
			flySpeed=  rand.nextInt(17+1-7)+7;
		}

		if (flyX<=0){

			gamestate=3;
			batch.draw(gameover,Gdx.graphics.getWidth()/2-gameover.getWidth()/2, Gdx.graphics.getHeight()/2-gameover.getHeight()/2);
		}


		scoreString=String.valueOf(score);
		bitmapFont.draw(batch,scoreString,100,200);

		batch.end();

	}
}
